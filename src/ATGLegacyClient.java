import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.cookie.Cookie;
import org.apache.http.cookie.CookieOrigin;
import org.apache.http.cookie.CookieSpec;
import org.apache.http.cookie.CookieSpecProvider;
import org.apache.http.cookie.MalformedCookieException;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.impl.cookie.BrowserCompatSpec;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;


public class ATGLegacyClient {
	
	String sessionConf = null;
	String host;
	int port;
	boolean useHTTPS;
	
	
	CloseableHttpClient defaultHttpClient = null;
	
	CookieStore cs = new BasicCookieStore();
	
	RequestConfig reqCfg = RequestConfig.custom()
	        .setSocketTimeout(1000000)
	        .setConnectTimeout(1000000)
	        .setCookieSpec("easy")
	        .build();
	
	// Stop warnings (and re-transmit) cookies w/ diff domain attribs
    CookieSpecProvider csf = new CookieSpecProvider() {
        @Override
        public CookieSpec create(HttpContext context)
        {
            return new BrowserCompatSpec() {
                @Override
                public void validate(Cookie cookie, CookieOrigin origin)
                    throws MalformedCookieException
                {
                    // Allow all cookies
                }
            };
        }
    };
    
    public ATGLegacyClient(String hostName, int portNum, boolean usesHTTPS)
    {
    	host = hostName;
    	port = portNum;
    	useHTTPS = usesHTTPS;
    	createClientNoAgent();
    }
	
	private  void createClientNoAgent() 
	{
		defaultHttpClient = HttpClientBuilder.create()
				.setRedirectStrategy(new LaxRedirectStrategy())
				.setDefaultCookieStore(cs)
	            .setDefaultCookieSpecRegistry(RegistryBuilder.<CookieSpecProvider>create()
                        .register(CookieSpecs.BEST_MATCH, csf)
                        .register(CookieSpecs.BROWSER_COMPATIBILITY, csf)
                        .register("easy", csf).build())
				.build();

	}
	// Put together the URL for calling ATG 
	private String buildURL(String path)
	{
		return (useHTTPS ? "https://" : "http://") + host + ":" + port + path;
	}

	
	private void getSessionConf() throws ClientProtocolException, IOException, JSONException
	{
		HttpGet sessGet = new HttpGet(buildURL("/rest/bean/atg/rest/SessionConfirmation/sessionConfirmationNumber?atg-rest-output=json"));
		sessGet.setConfig(reqCfg);
		
		CloseableHttpResponse response = defaultHttpClient.execute(sessGet);
		HttpEntity entity = response.getEntity();
		String respStr = EntityUtils.toString(entity);
		
		EntityUtils.consume(entity);
		JSONObject obj = new JSONObject(respStr);
		sessionConf = obj.getString("sessionConfirmationNumber");
		
		response.close();
		
	}
	

	// Do our multi-step login
	// First get the session confirmation #
	// Next, log in to the ATG URL 
	// (we will be considered a BOT when we do this, because of our user-agent string
	// However, if we try to change our user-agent string, we can't log in to the ATG URL)
	// Finally, log into the Ferguson user profile form handler (this registers us as a legitimate user
	// and lets us add items to the cart).
	// Any deviation from this will probably fail.
	public void doLogin(String username, String password) throws ClientProtocolException, IOException, JSONException
	{
		getSessionConf();
		doATGLogin(username, password);
		doFGFormLogin(username, password);
	}
	
	private void doFGFormLogin(String user, String pass) throws ClientProtocolException, IOException, JSONException
	{
	
		HttpPost loginPost = new HttpPost(buildURL("/rest/bean/ferguson/userprofiling/ProfileAuthenticationFormHandler/login"));

		loginPost.setConfig(reqCfg);
		JSONObject loginParams = new JSONObject();
		loginParams.put("_dynSessConf", sessionConf);
		loginParams.put("value.login", user);
		loginParams.put("value.password", pass);
		loginParams.put("atg-rest-output", "json");
		loginParams.put("loginRESTCall", "true");
		loginPost.addHeader(HttpHeaders.CONTENT_TYPE, "application/json");
		loginPost.addHeader(HttpHeaders.ACCEPT, "application/json");
	    loginPost.setEntity(new StringEntity(loginParams.toString()));

	    CloseableHttpResponse response = defaultHttpClient.execute(loginPost);
	    HttpEntity entity = response.getEntity();

	    // This is a true/false return (unlike the ATG login which is a null or profile ID return)
	    // We could check for a non-200 response here as well (and would probably be a good idea in production)
	    String respStr = EntityUtils.toString(entity);
		
		EntityUtils.consume(entity);
	
		response.close();
		String success = getATGResponse(respStr);
		if (success.equals("false"))
		{
			System.out.println("Unable to login on form");
			throw new IOException("could not log in to form");
		}

		
	}
	
	
	private void doATGLogin(String user, String pass) throws ClientProtocolException, IOException, JSONException
	{
		// login and get the JSESSION cookie
		
		HttpPost loginPost = new HttpPost(buildURL("/rest/bean/atg/userprofiling/ProfileServices/loginUser"));

		loginPost.setConfig(reqCfg);
		JSONObject loginParams = new JSONObject();
		loginParams.put("_dynSessConf", sessionConf);
		loginParams.put("arg1", user);
		loginParams.put("arg2", pass);
		loginParams.put("atg-rest-output", "json");
		loginPost.addHeader(HttpHeaders.CONTENT_TYPE, "application/json");
		loginPost.addHeader(HttpHeaders.ACCEPT, "application/json");
	    loginPost.setEntity(new StringEntity(loginParams.toString()));

	    CloseableHttpResponse response = defaultHttpClient.execute(loginPost);
	    HttpEntity entity = response.getEntity();
		
	    // We get 200/OK regardless of a successful log in or not. 
	    // get null if a bad login, good login returns profileID
	    // We could check for a non-200 response here as well
	    // (and probably should in production)
	    String respStr = EntityUtils.toString(entity);
		
		EntityUtils.consume(entity);
		
		response.close();
		String userID = getATGResponse(respStr);
		if (userID.equals("null"))
		{
			System.out.println("Unable to login to ATG");
			throw new IOException("could not log in to ATG");
		}
	}
	
	public void doLogout() throws ParseException, IOException
	{
		
		HttpPost logout = new HttpPost(buildURL("/rest/bean/atg/userprofiling/ProfileServices/logoutUser"));
		CloseableHttpResponse response = defaultHttpClient.execute(logout);
		HttpEntity entity = response.getEntity();
		// Response is void .. 
		String respStr = EntityUtils.toString(entity);
		
		EntityUtils.consume(entity);
		response.close();
	}
	
	public boolean addItemToCart(String sku, int quantity) throws ClientProtocolException, IOException, JSONException
	{
		String item = quantity + " " + sku;
		if (postCart(item) == null)
		{
			return true;
		}
		return false;
	}

	// Returns the value of the invalid SKU header. Null means the SKUs were accepted
	// Non null means the skus that were rejected will be returned.
	// Right now, we're only testing with single SKU and count combos (see addItemToCart above).
	// This could be made public and you call it with the formatted list, and would still work
	// returning a list of invalid skus
	// just note the comment about multiple inputs below:
	protected String postCart(String items) throws ClientProtocolException, IOException, JSONException
	{
		// Now, try to add something to the cart for the user via the existing form handler:
		HttpPost cartPost = new HttpPost(buildURL("/rest/bean/atg/commerce/order/purchase/CartModifierFormHandler/quickOrder"));
		cartPost.setConfig(reqCfg);
		

		List<NameValuePair> params = new ArrayList<NameValuePair>();
	    params = new ArrayList<NameValuePair>();
	    // The validator is VERY PICKY about the input
	    // if multiple inputs, needs to be  "5 c9d\r 5 c9b" -- note just a single \r AND where the spaces are/are not
	    params.add(new BasicNameValuePair("quickOrderInput", items));
	    params.add(new BasicNameValuePair("quickOrderREST", "true"));
	    params.add(new BasicNameValuePair("_dynSessConf", sessionConf));
	    cartPost.setEntity(new UrlEncodedFormEntity(params));
	    
		CloseableHttpResponse response = defaultHttpClient.execute(cartPost);
		HttpEntity entity = response.getEntity();
		
		String respStr = EntityUtils.toString(entity);
		EntityUtils.consume(entity);

		// We note good/bad skus in the headers and the order # (to look for the BOT tagging)
		// The order # is in the X-ORDER-NUMBER header
        Header h = response.getFirstHeader("X-INVALID-SKU");
        String invalidHeader = null;
        if (h != null) invalidHeader = h.getValue();
        response.close();
        return invalidHeader;
	}
	
	protected String getATGResponse(String respStr) throws JSONException
	{
		JSONObject obj = new JSONObject(respStr);
		return obj.getString("atgResponse");
	}
	
	protected void dumpHeaders(Header[] heads)
	{
        System.out.println("Headers: ");
        for (Header h : heads)
        {
        	System.out.println("\t" + h.getName() + ":" + h.getValue());
        }
	}
	
	protected void dumpCookies() {
		System.out.println("Cookies: ");
        List<Cookie> cookies = cs.getCookies();
        if (cookies.isEmpty()) {
            System.out.println("None");
        } else {
            for (int i = 0; i < cookies.size(); i++) {
                System.out.println("\t" + cookies.get(i).toString());
            }
        }
	}

}
