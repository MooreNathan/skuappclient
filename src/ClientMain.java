import java.io.IOException;

import org.apache.http.ParseException;
import org.json.JSONException;

public class ClientMain {

	public static void main(String[] args) throws ParseException, IOException, JSONException {
		
		// The biggest TODO: what are the values for production ? 
		// Do we need to open firewall ports and such to allow this access ?
		// Does this work with HTTPS ? (my dev vm ONLY does http)
		// How do we secure the REST endpoints ? 
		ATGLegacyClient cl = new ATGLegacyClient("www.localdev.ferguson.com", 8180, false);

		cl.doLogin("t3@ferguson.com", "Password1");
		if (cl.addItemToCart("c9d", 5)) {
			System.out.println("Added c9d to cart successfully");
		} else {
			System.out.println("Failed to add c9d to cart ");
			
		}
		
		if (cl.addItemToCart("sdfgsdgf", 5)) {
			System.out.println("Added sdfgsdgf to cart successfully");
		} else 
		{
			System.out.println("Failed to add sdfgsdgf to cart");
		}
		cl.doLogout();
		
	}

}
